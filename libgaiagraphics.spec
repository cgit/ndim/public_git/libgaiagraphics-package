Name:           libgaiagraphics
Version:        0.4
Release:        1.1%{?dist}
Summary:        Graphics canvas for GIS rendering

Group:          System Environment/Libraries
License:        GPLv3+
URL:            http://www.gaia-gis.it/spatialite-2.4.0-4
Source0:        http://www.gaia-gis.it/spatialite-2.4.0-4/libgaiagraphics-0.4.tar.gz
BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)

BuildRequires:  libjpeg-devel libpng-devel
BuildRequires:  zlib-devel
BuildRequires:  proj-devel
BuildRequires:  cairo-devel
# The source tarball explicitly mentions >= 1.2.5
BuildRequires:  libgeotiff-devel >= 1.2.5
BuildRequires:  pkgconfig

%description
Libgaigraphics wraps raster- and vector graphics, to implement a reasonably
abstract and platform independent graphics canvas for GIS rendering.

%package        devel
Summary:        Development files for %{name}
Group:          Development/Libraries
Requires:       %{name} = %{version}-%{release}

%description    devel
The %{name}-devel package contains libraries and header files for
developing applications that use %{name}.


%prep
%setup -q


%build
%configure --disable-static CPPFLAGS="$(pkg-config --cflags libgeotiff)"

# Remove links to unused libraries
sed -i -e 's! -shared ! -Wl,--as-needed\0!g' libtool

make %{?_smp_mflags}


%install
rm -rf %{buildroot}
make install DESTDIR=%{buildroot}

# Delete libtool archives, because we don't ship them.
find %{buildroot} -name '*.la' -exec rm -f {} ';'


%clean
rm -rf %{buildroot}


%post -p /sbin/ldconfig

%postun -p /sbin/ldconfig


%files
%defattr(-,root,root,-)
%doc AUTHORS COPYING
%{_libdir}/*.so.*

%files devel
%defattr(-,root,root,-)
%{_includedir}/*
%{_libdir}/*.so
%{_libdir}/pkgconfig/*

%changelog
* Tue Jan 18 2011 Hans Ulrich Niedermann <hun@n-dimensional.de> - 0.4-1.1
- Replace complicated geotiff patch with CPPFLAGS=

* Tue Jan 18 2011 Volker Fröhlich <volker27@gmx.at> - 0.4-1
- Initial packaging
